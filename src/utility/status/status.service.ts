import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { StatusDocument } from "./schema/status.schema";
import { CreateStatusDTO } from "./dto/status.dto";

@Injectable()
export class StatusService {

    constructor(@InjectModel('Status') private statusModel: Model<StatusDocument>) {}

    async getStatusList(): Promise<StatusDocument[]> {
        const statusList = await this.statusModel.find();
        return statusList;
    }

    async getStatusByType(statusType: string): Promise<StatusDocument[]> {
        const statusList = await this.statusModel.find({ 'type': statusType });
        return statusList;
    }

    async getStatus(statusId: string): Promise<StatusDocument> {
        const status = await this.statusModel.findById(statusId);
        return status;
    }

    async getStatusByCode(code: string): Promise<StatusDocument> {
        const status = await this.statusModel.findOne({ 'code': code });
        return status;
    }

    async createStatus(createStatusDTO: CreateStatusDTO): Promise<StatusDocument> {
        const status = new this.statusModel(createStatusDTO);
        await status.save();
        return status;
    }

    async updateStatus(statusId: string, createStatusDTO: CreateStatusDTO): Promise<StatusDocument> {
        const updatedStatus = await this.statusModel.findByIdAndUpdate(statusId, createStatusDTO, { new: true });
        return updatedStatus;
    }

    async deleteStatus(statusId: string): Promise<StatusDocument> {
        const statusDeleted = await this.statusModel.findByIdAndDelete(statusId);
        return statusDeleted;
    }

}
