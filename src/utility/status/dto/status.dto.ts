export class CreateStatusDTO {
    readonly name: string;
    readonly code: string;
    readonly type: string;
}