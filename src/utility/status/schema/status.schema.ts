import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type StatusDocument = Status & Document;

@Schema({ versionKey: false })
export class Status {
    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: String, required: true })
    code: string;

    @Prop({ type: String, required: true })
    type: string;
}

export const StatusSchema = SchemaFactory.createForClass(Status);