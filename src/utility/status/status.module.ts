import { Module } from '@nestjs/common';
import { StatusService } from './status.service';
import { StatusController } from './status.controller';
import { MongooseModule } from "@nestjs/mongoose";
import { StatusSchema } from "./schema/status.schema";
import { AppService } from "src/app.service";

@Module({
  imports: [ MongooseModule.forFeature([
    { name: 'Status', schema: StatusSchema }
  ])],
  providers: [StatusService, AppService],
  controllers: [StatusController]
})
export class StatusModule {}
