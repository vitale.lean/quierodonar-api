import { Controller, Get, Post, Put, Delete, Res, Body, Param, HttpStatus, NotFoundException, UseGuards } from '@nestjs/common';
import { CreateStatusDTO } from "./dto/status.dto";
import { StatusService } from "./status.service";
import { JWTAuthGuard } from "src/guards/jwt.guard";

@Controller('status')
export class StatusController {

    constructor(private statusService: StatusService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getStatusList(@Res() res) {
        const statusList = await this.statusService.getStatusList();
        return res.status(HttpStatus.OK).json({
            data: statusList
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getStatus(@Res() res, @Param('id') id) {
        const status = await this.statusService.getStatus(id);
        if ( !status ) throw new NotFoundException('Status Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: status
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/type/:type')
    async getStatusByType(@Res() res, @Param('type') type) {
        const statusList = await this.statusService.getStatusByType(type);
        return res.status(HttpStatus.OK).json({
            data: statusList
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/code/:code')
    async getStatusByCode(@Res() res, @Param('code') code) {
        const status = await this.statusService.getStatusByCode(code);
        if ( !status ) throw new NotFoundException('Status Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: status
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createDonor(@Res() res, @Body() createStatusDTO: CreateStatusDTO) {
        const status = await this.statusService.createStatus(createStatusDTO);
        return res.status(HttpStatus.OK).json({
            data: status
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateDonor(@Res() res, @Param('id') id, @Body() createStatusDTO: CreateStatusDTO) {
        const statusUpdated = await this.statusService.updateStatus(id, createStatusDTO);
        if ( !statusUpdated ) throw new NotFoundException('Status Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: statusUpdated
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteDonor(@Res() res, @Param('id') id) {
        const statusDeleted = await this.statusService.deleteStatus(id);
        if ( !statusDeleted ) throw new NotFoundException('Status Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }

}
