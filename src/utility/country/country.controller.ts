import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from "src/guards/jwt.guard";
import { CountryService } from "./country.service";
import { CreateCountryDTO } from "./dto/country.dto";

@Controller('country')
export class CountryController {

    constructor(private countryService: CountryService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getCountrys(@Res() res) {
        const countrys = await this.countryService.getCountrys();
        return res.status(HttpStatus.OK).json({
            data: countrys
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getEventType(@Res() res, @Param('id') id) {
        const country = await this.countryService.getCountry(id);
        if ( !country ) throw new NotFoundException('Country Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: country
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createEventType(@Res() res, @Body() createCountryDTO: CreateCountryDTO) {
        const country = await this.countryService.createCountry(createCountryDTO);
        return res.status(HttpStatus.OK).json({
            data: country
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateEventType(@Res() res, @Param('id') id,  @Body() createCountryDTO: CreateCountryDTO) {
        const country = await this.countryService.updateCountry(id, createCountryDTO);
        if ( !country ) throw new NotFoundException('Country Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: country
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteEventType(@Res() res, @Param('id') id) {
        const country = await this.countryService.deleteCountry(id);
        if ( !country ) throw new NotFoundException('Country Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }
}
