import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { CountryDocument } from "./schema/country.schema";
import { CreateCountryDTO } from "./dto/country.dto";

@Injectable()
export class CountryService {

    constructor(@InjectModel('Country') private countryModel: Model<CountryDocument>) {}

    async getCountry(countryID: string): Promise<CountryDocument> {
        const country = await this.countryModel.findById(countryID);
        return country;
    }

    async getCountrys(): Promise<CountryDocument[]> {
        const countrys = await this.countryModel.find();
        return countrys;
    }

    async createCountry(createCountryDTO: CreateCountryDTO): Promise<CountryDocument> {
        const country = new this.countryModel(createCountryDTO);
        await country.save();
        return country;
    }

    async updateCountry(countryID: string, createCountryDTO: CreateCountryDTO): Promise<CountryDocument> {
        const country = await this.countryModel.findByIdAndUpdate(countryID, createCountryDTO, { new: true });
        return country;
    }

    async deleteCountry(countryID: string): Promise<CountryDocument> {
        const country = await this.countryModel.findByIdAndDelete(countryID);
        return country;
    }

}
