import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type CountryDocument = Country & Document;

@Schema({ versionKey: false })
export class Country {
    @Prop({ type: String, required: true })
    name: string;
}

export const CountrySchema = SchemaFactory.createForClass(Country);