import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from 'src/app.service';
import { CountryController } from './country.controller';
import { CountryService } from './country.service';
import { CountrySchema } from "./schema/country.schema";

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Country', schema: CountrySchema }
  ])],
  controllers: [CountryController],
  providers: [CountryService, AppService]
})
export class CountryModule {}
