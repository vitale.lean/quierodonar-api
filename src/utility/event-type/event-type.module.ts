import { Module } from '@nestjs/common';
import { EventTypeController } from './event-type.controller';
import { EventTypeService } from './event-type.service';
import { MongooseModule } from "@nestjs/mongoose";
import { EventTypeSchema } from "./schema/event-type.schema";
import { AppService } from 'src/app.service';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'EventType', schema: EventTypeSchema }
  ])],
  controllers: [EventTypeController],
  providers: [EventTypeService, AppService]
})
export class EventTypeModule {}
