import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { EventTypeDocument } from "./schema/event-type.schema";
import { CreateEventTypeDTO } from "./dto/event-type.dto";

@Injectable()
export class EventTypeService {

    constructor(@InjectModel('EventType') private eventTypeModel: Model<EventTypeDocument>) {}

    async getEventType(eventTypeID: string): Promise<EventTypeDocument> {
        const eventType = await this.eventTypeModel.findById(eventTypeID);
        return eventType;
    }

    async getEventTypes(): Promise<EventTypeDocument[]> {
        const eventTypes = await this.eventTypeModel.find();
        return eventTypes;
    }

    async createEventType(createEventTypeDTO: CreateEventTypeDTO): Promise<EventTypeDocument> {
        const eventType = new this.eventTypeModel(createEventTypeDTO);
        await eventType.save();
        return eventType;
    }

    async updateEventType(eventTypeID: string, createEventTypeDTO: CreateEventTypeDTO): Promise<EventTypeDocument> {
        const eventType = await this.eventTypeModel.findByIdAndUpdate(eventTypeID, createEventTypeDTO, { new: true });
        return eventType;
    }

    async deleteEventType(eventTypeID: string): Promise<EventTypeDocument> {
        const eventType = await this.eventTypeModel.findByIdAndDelete(eventTypeID);
        return eventType;
    }

}
