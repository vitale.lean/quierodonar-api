import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EventTypeDocument = EventType & Document;

@Schema({ versionKey: false })
export class EventType {
    @Prop({ type: String, required: true })
    name: string;
}

export const EventTypeSchema = SchemaFactory.createForClass(EventType);