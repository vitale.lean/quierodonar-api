import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from "src/guards/jwt.guard";
import { CreateEventTypeDTO } from "./dto/event-type.dto";
import { EventTypeService } from "./event-type.service";

@Controller('event-type')
export class EventTypeController {

    constructor(private etService: EventTypeService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getEventTypes(@Res() res) {
        const eventTypes = await this.etService.getEventTypes();
        return res.status(HttpStatus.OK).json({
            data: eventTypes
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getEventType(@Res() res, @Param('id') id) {
        const eventType = await this.etService.getEventType(id);
        if ( !eventType ) throw new NotFoundException('EventType Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: eventType
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createEventType(@Res() res, @Body() createEventTypeDTO: CreateEventTypeDTO) {
        const eventType = await this.etService.createEventType(createEventTypeDTO);
        return res.status(HttpStatus.OK).json({
            data: eventType
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateEventType(@Res() res, @Param('id') id,  @Body() createEventTypeDTO: CreateEventTypeDTO) {
        const updatedEventType = await this.etService.updateEventType(id, createEventTypeDTO);
        if ( !updatedEventType ) throw new NotFoundException('EventType Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: updatedEventType
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteEventType(@Res() res, @Param('id') id) {
        const eventType = await this.etService.deleteEventType(id);
        if ( !eventType ) throw new NotFoundException('EventType Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }

}
