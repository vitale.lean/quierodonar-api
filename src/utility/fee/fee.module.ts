import { Module } from '@nestjs/common';
import { FeeService } from './fee.service';
import { FeeController } from './fee.controller';
import { AppService } from 'src/app.service';
import { MongooseModule } from "@nestjs/mongoose";
import { FeeSchema } from "./schema/fee.schema";

@Module({
  imports: [ MongooseModule.forFeature([
    { name: 'Fee', schema: FeeSchema }
  ]) ],
  providers: [FeeService, AppService],
  controllers: [FeeController]
})
export class FeeModule {}
