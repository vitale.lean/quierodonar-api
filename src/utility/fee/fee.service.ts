import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { FeeDocument } from "./schema/fee.schema";
import { CreateFeeDTO } from "./dto/fee.dto";

@Injectable()
export class FeeService {

    constructor(@InjectModel('Fee') private feeModel: Model<FeeDocument>) {}

    async getFees(): Promise<FeeDocument[]> {
        const fees = await this.feeModel.find();
        return fees;
    }

    async getFee(feeId: string): Promise<FeeDocument> {
        const fee = await this.feeModel.findById(feeId);
        return fee;
    }

    async createFee(createFeeDTO: CreateFeeDTO): Promise<FeeDocument> {
        const fee = new this.feeModel(createFeeDTO);
        await fee.save();
        return fee;
    }

    async updateFee(feeId: string, createFeeDTO: CreateFeeDTO): Promise<FeeDocument> {
        const fee = await this.feeModel.findByIdAndUpdate(feeId, createFeeDTO, { new: true });
        return fee;
    }

    async deleteFee(feeId: string): Promise<FeeDocument> {
        const fee = await this.feeModel.findByIdAndDelete(feeId);
        return fee;
    }

}
