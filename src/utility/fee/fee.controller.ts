import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { FeeService } from "./fee.service";
import { CreateFeeDTO } from "./dto/fee.dto";
import { JWTAuthGuard } from "src/guards/jwt.guard";


@Controller('fee')
export class FeeController {

    constructor(private feeService: FeeService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getFees(@Res() res) {
        const fees = await this.feeService.getFees();
        return res.status(HttpStatus.OK).json({
            data: fees
        });
    } 

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getFee(@Res() res, @Param('id') id) {
        const fee = await this.feeService.getFee(id);
        if ( !fee ) throw new NotFoundException('Fee Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: fee
        });
    } 

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createFee(@Res() res, @Body() createFeeDTO: CreateFeeDTO) {
        const fee = await this.feeService.createFee(createFeeDTO);
        return res.status(HttpStatus.OK).json({
            data: fee
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateFee(@Res() res, @Param('id') id, @Body() createFeeDTO: CreateFeeDTO) {
        const fee = await this.feeService.updateFee(id, createFeeDTO);
        if ( !fee ) throw new NotFoundException('Fee Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: fee
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteFee(@Res() res, @Param('id') id) {
        const fee = await this.feeService.deleteFee(id);
        if ( !fee ) throw new NotFoundException('Fee Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({})
    }

}
