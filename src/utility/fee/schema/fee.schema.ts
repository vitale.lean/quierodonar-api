import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type FeeDocument = Fee & Document;

@Schema({ versionKey: false })
export class Fee {
    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: Number, required: true })
    fee: number;
}

export const FeeSchema = SchemaFactory.createForClass(Fee);