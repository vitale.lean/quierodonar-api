export class CreateFeeDTO {
    name: string;
    fee: number;
}