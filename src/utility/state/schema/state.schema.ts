import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, Types } from "mongoose";
import * as mongoose from "mongoose";

export type StateDocument = State & Document;

@Schema({ versionKey: false })
export class State {
    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Country', required: true })
    country: Types.ObjectId;
}

export const StateSchema = SchemaFactory.createForClass(State);