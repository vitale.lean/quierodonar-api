import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from 'src/app.service';
import { StateController } from './state.controller';
import { StateService } from './state.service';
import { StateSchema } from "./schema/state.schema";

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'State', schema: StateSchema }
  ])],
  controllers: [StateController],
  providers: [StateService, AppService]
})
export class StateModule {}
