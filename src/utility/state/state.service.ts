import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { StateDocument } from "./schema/state.schema";
import { CreateStateDTO } from "./dto/state.dto";

@Injectable()
export class StateService {

    constructor(@InjectModel('State') private stateModel: Model<StateDocument>) {}

    async getStates(): Promise<StateDocument[]> {
        const states = await this.stateModel.find()
                                            .populate('country');
        return states;
    }

    async getStateByID(stateID: string): Promise<StateDocument> {
        const state = await this.stateModel.findById(stateID);
        return state;
    }

    async getStatesByCountry(countryID: string): Promise<StateDocument[]> {
        const states = await this.stateModel.find({ country: countryID });
        return states;
    }

    async createState(createStateDTO: CreateStateDTO): Promise<StateDocument> {
        const state = new this.stateModel(createStateDTO);
        await state.save();
        return state;
    }

    async updateState(stateID: string, createStateDTO: CreateStateDTO): Promise<StateDocument> {
        const state = await this.stateModel.findByIdAndUpdate(stateID, createStateDTO, { new: true });
        return state;
    }

    async deleteState(stateID: string): Promise<StateDocument> {
        const state = await this.stateModel.findByIdAndDelete(stateID);
        return state;
    }

}
