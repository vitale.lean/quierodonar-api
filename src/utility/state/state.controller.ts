import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from "src/guards/jwt.guard";
import { CreateStateDTO } from "./dto/state.dto";
import { StateService } from "./state.service";

@Controller('state')
export class StateController {

    constructor(private stateSerive: StateService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getStates(@Res() res) {
        const states = await this.stateSerive.getStates();
        return res.status(HttpStatus.OK).json({
            data: states
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getState(@Res() res, @Param('id') id) {
        const state = await this.stateSerive.getStateByID(id);
        if ( !state ) throw new NotFoundException('State Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: state
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/country/:id')
    async getStatesByCountry(@Res() res, @Param('id') id) {
        const states = await this.stateSerive.getStatesByCountry(id);
        return res.status(HttpStatus.OK).json({
            data: states
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createState(@Res() res, @Body() createStateDTO: CreateStateDTO) {
        const state = await this.stateSerive.createState(createStateDTO);
        return res.status(HttpStatus.OK).json({
            data: state
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateState(@Res() res, @Param('id') id,  @Body() createStateDTO: CreateStateDTO) {
        const state = await this.stateSerive.updateState(id, createStateDTO);
        if ( !state ) throw new NotFoundException('State Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: state
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteState(@Res() res, @Param('id') id) {
        const state = await this.stateSerive.deleteState(id);
        if ( !state ) throw new NotFoundException('State Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }

}
