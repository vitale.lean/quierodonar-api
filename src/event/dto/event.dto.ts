export class CreateEventDTO {
    name: string;
    alias: string;
    smallDescription: string;
    largeDescription: string;
    startDate: Date;
    endDate: Date;
    image: string;
    objetive: number;
    collected: number;
    eventType: string;
    foundation: string;
    status: string;
    deleted: boolean;
    created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}

export class ParamsEventDTO {
    eventType: string;
    foundation: string;
    status: string;
}