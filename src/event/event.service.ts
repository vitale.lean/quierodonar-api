import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { EventDocument } from "./schema/event.schema";
import { CreateEventDTO, ParamsEventDTO } from "./dto/event.dto";

@Injectable()
export class EventService {

    constructor(@InjectModel('Event') private eventModel: Model<EventDocument>) {}

    async getEvents(): Promise<EventDocument[]> {
        const events = await this.eventModel.find({ deleted: false })
                                            .populate('eventType')
                                            .populate('status')
                                            .populate('foundation', 'name');
        return events;
    }

    async getEventByID(eventID: string): Promise<EventDocument> {
        const event = await this.eventModel.findById(eventID);
        return event;
    }

    async getEventByAlias(eventAlias: string): Promise<EventDocument> {
        const event = await this.eventModel.findOne({ alias: eventAlias });
        return event;
    }

    async createEvent(createEventDTO: CreateEventDTO): Promise<EventDocument> {
        const event = new this.eventModel(createEventDTO);
        await event.save();
        return event;
    }

    async updateEvent(eventId: string, createEventDTO: CreateEventDTO): Promise<EventDocument> {
        createEventDTO.updated_at = new Date();
        const eventUpdated = await this.eventModel.findByIdAndUpdate(eventId, createEventDTO, { new: true });
        return eventUpdated;
    }

    async deleteEvent(eventId: string): Promise<EventDocument> {
        const event = await this.getEventByID(eventId);
        event.deleted_at = new Date();
        event.updated_at = new Date();
        event.deleted = true;
        const eventSoftDelete = await this.eventModel.findByIdAndUpdate(eventId, event, { new: true });
        return eventSoftDelete;
    }

}
