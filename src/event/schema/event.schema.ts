import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose'; 
import { Document, Types } from 'mongoose';

export type EventDocument = Event & Document;

@Schema({ versionKey: false })
export class Event {
    @Prop()
    name: string;

    @Prop()
    alias: string;

    @Prop()
    smallDescription: string;

    @Prop()
    largeDescription: string;

    @Prop()
    startDate: Date;

    @Prop()
    endDate: Date;

    @Prop()
    image: string;

    @Prop()
    objetive: number;

    @Prop({ type: Boolean, default: false })
    deleted: boolean;

    @Prop({ type: Number, default: 0 })
    collected: number;

    @Prop({ type: Date, default: Date.now })
    created_at: Date;
    
    @Prop({ type: Date, default: Date.now })
    updated_at: Date;
    
    @Prop({ type: Date })
    deleted_at: Date;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'EventType' })
    eventType: Types.ObjectId;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Foundation' })
    foundation: Types.ObjectId;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Status' })
    status: Types.ObjectId;
}

export const EventSchema = SchemaFactory.createForClass(Event);