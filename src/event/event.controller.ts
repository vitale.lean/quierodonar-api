import { Body, Controller, Delete, Get, HttpStatus, NotFoundException, Param, Post, Put, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from "src/guards/jwt.guard";
import { CreateEventDTO } from "./dto/event.dto";
import { EventService } from "./event.service";


@Controller('event')
export class EventController {

    constructor(private eventService: EventService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getEvents(@Res() res) {
        const events = await this.eventService.getEvents();
        return res.status(HttpStatus.OK).json({
            data: events
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getEventById(@Res() res, @Param('id') id) {
        const event = await this.eventService.getEventByID(id);
        if ( !event ) throw new NotFoundException('Event Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: event
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/alias/:alias')
    async getEventByAlias(@Res() res, @Param('alias') alias) {
        const event = await this.eventService.getEventByAlias(alias);
        if ( !event ) throw new NotFoundException('Event Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: event
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createEvent(@Res() res, @Body() createEventDTO: CreateEventDTO) {
        const event = await this.eventService.createEvent(createEventDTO);
        return res.status(HttpStatus.OK).json({
            data: event
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update/:id')
    async updateEvent(@Res() res, @Param('id') id, @Body() createEventDTO: CreateEventDTO) {
        const event = await this.eventService.updateEvent(id, createEventDTO);
        if ( !event ) throw new NotFoundException('Event Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: event
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete/:id')
    async deleteEvent(@Res() res, @Param('id') id) {
        const event = await this.eventService.deleteEvent(id);
        if ( !event ) throw new NotFoundException('Event Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }
}
