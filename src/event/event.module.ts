import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from 'src/app.service';
import { EventController } from './event.controller';
import { EventService } from './event.service';
import { EventSchema } from "./schema/event.schema";

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Event', schema: EventSchema }
  ])],
  controllers: [EventController],
  providers: [EventService, AppService]
})
export class EventModule {}
