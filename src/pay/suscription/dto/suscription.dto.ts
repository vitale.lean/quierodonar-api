export class CreateSuscriptionDTO {
    donor: string;
    foundation: string;
    amount: number;
}