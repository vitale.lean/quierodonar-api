import { Injectable } from '@nestjs/common';
import { DonorService } from 'src/donor/donor.service';
import { FoundationService } from 'src/foundation/foundation.service';
import { StripeService } from 'src/services/stripe.service';
import { OperationService } from '../operation/operation.service';
import { CreateSuscriptionDTO } from './dto/suscription.dto';

@Injectable()
export class SuscriptionService {

    constructor(private donorService: DonorService,
                private foundationService: FoundationService,
                private operationService: OperationService,
                private stripeService: StripeService) { }

    async createSuscriptionIntent(suscriptionDTO: CreateSuscriptionDTO): Promise<any> {

        // Get Foundation Data
        const foundation = await this.foundationService
                                        .getFoundation(suscriptionDTO.foundation, 'stripe_id fee email name', 'fee');

        // Get Donor Data
        let donor = await this.donorService.getDonorByUid(suscriptionDTO.donor);

        if ( !donor.stripe_id ) {            
            const accCustomer = await this.stripeService.createCustomer(donor.email, donor.name, donor.phone);
            donor.stripe_id = accCustomer.id;
            donor = await this.donorService.updateDonor(donor.uid, donor);
        }

        const intent = await this.stripeService.createSuscription(suscriptionDTO.amount, foundation.stripe_id, donor.stripe_id);
        return intent;

    }
}
