import { Module } from '@nestjs/common';
import { SuscriptionService } from './suscription.service';
import { SuscriptionController } from './suscription.controller';
import { AppService } from 'src/app.service';
import { StripeService } from 'src/services/stripe.service';

@Module({
  providers: [SuscriptionService, AppService, StripeService],
  controllers: [SuscriptionController]
})
export class SuscriptionModule {}
