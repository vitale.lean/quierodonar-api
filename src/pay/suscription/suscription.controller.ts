import { Body, Controller, HttpStatus, NotFoundException, Param, Post, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from 'src/guards/jwt.guard';
import { CreateSuscriptionDTO } from './dto/suscription.dto';
import { SuscriptionService } from './suscription.service';

@Controller('suscription')
export class SuscriptionController {

    constructor(private suscriptionService: SuscriptionService) {}

    @UseGuards(JWTAuthGuard)
    @Post('/')
    async createPaymentIntent(@Res() res, @Param('uid') uid, @Body() createSuscriptionDTO: CreateSuscriptionDTO ) {
        try {
            createSuscriptionDTO.donor = uid;
            const suscriptionIntent = await this.suscriptionService.createSuscriptionIntent(createSuscriptionDTO);
            return res.status(HttpStatus.OK).json({
                subscriptionId: suscriptionIntent.id,
                secret: suscriptionIntent.latest_invoice.payment_intent.client_secret
            });
        } catch (err) {
            throw new NotFoundException(err);
        }
    }

    // TODO: Argregar EP para el Webhook que controla las suscripciones.
    @Post('/webhook')
    async stripeWebhookPayment(@Res() res) {
        return res.status(HttpStatus.OK).json({
            status: 'OK'
        });
    }

}
