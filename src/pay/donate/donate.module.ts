import { Module } from '@nestjs/common';
import { DonateService } from './donate.service';
import { DonateController } from './donate.controller';
import { AppService } from 'src/app.service';
import { StripeService } from 'src/services/stripe.service';

@Module({
  providers: [DonateService, AppService, StripeService],
  controllers: [DonateController]
})
export class DonateModule {}
