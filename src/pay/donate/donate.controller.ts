import { Body, Controller, HttpStatus, NotFoundException, Param, Post, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from 'src/guards/jwt.guard';
import { DonateService } from './donate.service';
import { CreateDonateDTO } from './dto/donate.dto';

@Controller('donate')
export class DonateController {

    constructor(private donateService: DonateService) {}

    @UseGuards(JWTAuthGuard)
    @Post('/')
    async createPaymentIntent(@Res() res, @Param('uid') uid, @Body() createDonateDTO: CreateDonateDTO) {
        try {
            createDonateDTO.donor = uid;
            const paymentIntent = await this.donateService.createPaymentIntent(createDonateDTO);
            return res.status(HttpStatus.OK).json({
                paymentIntent: true,
                secret: paymentIntent.client_secret
            });
        } catch (err) {
            throw new NotFoundException(err);
        }
    }
 
    // TODO: Argregar EP para el Webhook que controla las donaciones.
    @Post('/webhook')
    async stripeWebhookPayment(@Res() res) {
        return res.status(HttpStatus.OK).json({
            status: 'OK'
        });
    }

}
