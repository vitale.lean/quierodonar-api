export class CreateDonateDTO {
    donor: string;
    foundation: string;
    event: string;
    amount: number;
}