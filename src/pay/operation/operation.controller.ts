import { Body, Controller, Get, HttpStatus, NotFoundException, Post, Res, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from 'src/guards/jwt.guard';
import { CreateOperationDTO } from './dto/operation.dto';
import { OperationService } from './operation.service';


@Controller('operation')
export class OperationController {

    constructor(private opService: OperationService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getOperations(@Res() res) {
        const operations = await this.opService.getOperations();
        return res.status(HttpStatus.OK).json({
            data: operations
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post()
    async createOperation(@Res() res, @Body() body) {
        try {
            let createOperationDTO = new CreateOperationDTO();
            createOperationDTO.donor = '6020b3ab8c1a4608dc3c8539'; // Lean Vitale
            createOperationDTO.foundation = '60843c417e5ee607ae318359'; // Musonyes

            // MercadoPago Request Information
            createOperationDTO.log = body;
            createOperationDTO.status = body.status;
            createOperationDTO.amount = body.transaction_amount;

            const operation = await this.opService.createOperation(createOperationDTO);

            return res.status(HttpStatus.OK).json({
                created: true
            });
        } catch ( err ) {
            throw new NotFoundException(err);
        }
    }

}
