import { Global, Module } from '@nestjs/common';
import { OperationService } from './operation.service';
import { OperationController } from './operation.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { OperationSchema } from './schema/operation.schema';
import { AppService } from 'src/app.service';

@Global()
@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Operation', schema: OperationSchema },
  ])],
  providers: [OperationService, AppService],
  controllers: [OperationController],
  exports: [OperationService]
})
export class OperationModule {}
