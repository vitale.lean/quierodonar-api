export class CreateOperationDTO {
    foundation: string;
    event: string;
    donor: string;
    status: string;
    amount: number;
    id: number;
    currency_id: string;
    payment_method_id: string;
    date_created: Date;
    date_updated: Date;
    fee_details: Object;
    log: Object;
}