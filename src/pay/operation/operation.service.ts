import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { OperationDocument } from "./schema/operation.schema";
import { CreateOperationDTO } from "./dto/operation.dto";

@Injectable()
export class OperationService {

    constructor(@InjectModel('Operation') private opModel: Model<OperationDocument>) {}

    async createOperation(createOperationDTO: CreateOperationDTO): Promise<OperationDocument> {
        try {
            const operation = new this.opModel(createOperationDTO);
            await operation.save();
            return operation;
        } catch (err) {
            throw new NotFoundException(err);
        }
    }

    async getOperations(): Promise<OperationDocument[]> {
        const operations = await this.opModel.find().populate('foundation', 'name').populate('donor', 'name').select('-mercadopago');
        return operations;
    }

}
