import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from 'mongoose'; 

export type OperationDocument = Operation & Document;

@Schema({ versionKey: false })
export class Operation {
    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Foundation', required: true })
    foundation: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Donor', required: true })
    donor: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Event' })
    event: string;

    @Prop({ type: String , required: true })
    status: string;

    @Prop({ type: Number , required: true })
    amount: number;

    @Prop({ type: String , required: true })
    id: string;

    @Prop({ type: String , required: true })
    currency_id: string;

    @Prop({ type: String , required: true })
    payment_method_id: string;

    @Prop({ type: Date , required: true })
    date_created: Date;

    @Prop({ type: Date , required: true })
    date_updated: Date;

    @Prop({ type: Object, required: true })
    fee_details: Object;

    @Prop({ type: Object, required: true })
    log: Object;
}

export const OperationSchema = SchemaFactory.createForClass(Operation);