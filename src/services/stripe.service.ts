import { Injectable } from '@nestjs/common';
import { CreateDonorDTO } from 'src/donor/dto/donor.dto';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const stripe = require('stripe')('sk_test_51IlD9eFeMhjb8AhuYk7KliZ2zFfeGnvz9JCSBzUG5n8Pdf7Q4B1sY7qw7YRO1jcC5Nz8G1b37Jz9gO8drctOUNiz00hVgcAl0k');

@Injectable()
export class StripeService {

    // Get Foundation Acc Stripe
    async getAccount(stripeID: string) {
      const account = await stripe.accounts.retrieve( stripeID );
      return account;
    }

    // Create New Foundation Acc Stripe
    async createAccount(email: string) {
        const account = await stripe.accounts.create({
            type: 'express',
            country: 'ES',
            email,
            business_type: 'non_profit'
        });

        return account;
    }

    // Generate Link to Onboarding Account in Stripe
    async createLinkAccount(accountID: string) {
      const accountLink = await stripe.accountLinks.create({
        account: accountID,
        refresh_url: 'https://putsreq.com/JzB1pSL8lOQrrzk0aR3V',
        return_url: 'https://putsreq.com/JzB1pSL8lOQrrzk0aR3V',
        type: 'account_onboarding',
      });

      return accountLink;
    }

    // Get Customer Acc Stripe (Donor)
    async getCustomerAcc(stripeID: string) {
      const customer = await stripe.customers.retrieve( stripeID );
      return customer;
    }

    // Create New Customer in Stripe (Donor)
    async createCustomer(email: string, name: string, phone?: string) {
      const customer = await stripe.customers.create({
        email,
        name,
        phone
      });

      return customer;
    }

    // Update Customer's Information (Donor)
    async updateCustomer(donor: CreateDonorDTO, cardToken?: string) {
      const customer = await stripe.customers.update(
        donor.stripe_id,
        {
          source: cardToken
        }
      );

      return customer;
    }

    // Delete a Customer in Stripe (Donor)
    async deleteCustomer(donorID: string) {
      const deleted = await stripe.customers.del( donorID );
      return deleted;
    }

    async createCard(cusAcc: string, token: string) {
      const card = await stripe.customers.createSource(
        cusAcc,
        {source: token}
      );

      return card;
    }

    // Create a Payout
    async createPayout() {
      const payout = await stripe.payouts.create({
        amount: 1100,
        currency: 'usd',
      });
    }

    async createPaymentIntent(amount: number, stripeAcc: string, customerAcc: string) {
      
      const paymentIntent = await stripe.paymentIntents.create({
        payment_method_types: ['card'],
        amount: amount * 100,
        currency: 'eur',
        application_fee_amount: Math.round((amount * 0.03) * 100),
        customer: customerAcc,
        transfer_data: {
          destination: stripeAcc,
        },
        setup_future_usage: 'on_session'
      });
      
      return paymentIntent;
    }

    async createSuscriptionPrice(amount: number, foundationName: string) {
      const product = await stripe.products.create({
        name: `Suscripción de ${amount}Є a ${foundationName}`,
      });

      const price = await stripe.prices.create({
        unit_amount: amount * 100,
        currency: 'eur',
        recurring: {interval: 'month'},
        product: product.id,
      });
      
      return price;
    }

    async createSuscription(amount: number, stripeAcc: string, customerId: string) {
      const priceID = await this.createSuscriptionPrice(amount, 'Fundacion Mengele');

      const subscription = await stripe.subscriptions.create({
        customer: customerId,
        items: [{
          price: priceID.id,
        }],
        application_fee_percent: 3,
        payment_behavior: 'default_incomplete',
        expand: ['latest_invoice.payment_intent'],
        transfer_data: {
          destination: stripeAcc,
        }
      });

      return subscription;
    }

}
