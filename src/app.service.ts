import { Injectable } from '@nestjs/common';

// Firebase SDK
const firebaseAdmin = require('firebase-admin');
const serviceAccount = require('../firebase-auth/serviceAccountKey.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: process.env.URL_FIREBASE
});

@Injectable()
export class AppService {

  getHello(): string {
    return 'QuieroDonar API';
  }

  async checkJWT(idToken: string): Promise<any> {
    const jwt = await firebaseAdmin.auth().verifyIdToken(idToken);
    return jwt;
  }

}
