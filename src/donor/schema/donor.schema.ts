import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type DonorDocument = Donor & Document;

@Schema({ versionKey: false })
export class Donor {
    @Prop({ type: String, required: true })
    uid: string;

    @Prop({ type: String })
    stripe_id: string;

    @Prop({ type: String, required: true })
    name: string;

    @Prop({ type: String, required: true })
    email: string;

    @Prop({ type: String, required: true })
    phone: string;

    @Prop({ type: String, required: true })
    birthdate: string;

    @Prop({ type: String, required: true })
    gender: string;

    @Prop({ type: String })
    image: string;

    @Prop({ type: String, required: true })
    docNumber: string;

    @Prop({ type: String, required: true })
    docType: string;

    @Prop({ type: String })
    extra: string;

    @Prop({ type: String, required: true })
    alias: string;

    @Prop({ type: String, required: true })
    nickname: string;

    @Prop({ type: String, required: true })
    country: string;

    @Prop({ type: String, required: true })
    state: string;

    @Prop({ type: Number, required: true })
    userType: number;

    @Prop({ type: Boolean, required: false })
    actived: boolean;

    @Prop({ type: Boolean, required: false })
    deleted: boolean;

    @Prop({ type: Date, default: Date.now })
    created_at: Date;

    @Prop({ type: Date, default: Date.now })
    updated_at: Date;

    @Prop({ type: Date })
    deleted_at: Date;
}

export const DonorSchema = SchemaFactory.createForClass(Donor);