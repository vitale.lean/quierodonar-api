export class CreateDonorDTO {
    uid: string;
    readonly stripe_id: string;
    readonly name: string;
    readonly email: string;
    readonly phone: string;
    readonly birthdate: string;
    readonly gender: string;
    readonly image: string;
    readonly docNumber: string;
    readonly docType: string;
    readonly extra: string;
    readonly alias: string;
    readonly nickname: string;
    readonly country: string;
    readonly state: string;
    readonly userType: number;
    readonly actived: boolean;
    readonly deleted: boolean;
    readonly created_at: Date;
    updated_at: Date;
    deleted_at: Date;
}