import { Controller, Get, Post, Put, Delete, Res, Body, Param, HttpStatus, NotFoundException, UseGuards } from '@nestjs/common';
import { CreateDonorDTO } from "./dto/donor.dto";
import { DonorService } from "./donor.service";
import { JWTAuthGuard } from 'src/guards/jwt.guard';

@Controller('donor')
export class DonorController {

    constructor(private donorService: DonorService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getDonors(@Res() res) {
        const donors = await this.donorService.getDonors();
        return res.status(HttpStatus.OK).json({
            data: donors
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/:id')
    async getDonor(@Res() res, @Param('id') id) {
        const donor = await this.donorService.getDonor(id);
        if ( !donor ) throw new NotFoundException('Donor Does Not Exists')
        return res.status(HttpStatus.OK).json({
            data: donor
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/alias/:alias')
    async getDonorByAlias(@Res() res, @Param('alias') alias) {
        const donor = await this.donorService.getDonorByAlias(alias);
        if ( !donor ) throw new NotFoundException('Donor Does Not Exists')
        return res.status(HttpStatus.OK).json({
            data: donor
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createDonor(@Res() res, @Param('uid') uid, @Body() createDonorDTO: CreateDonorDTO) {
        createDonorDTO.uid = uid;
        const donor = await this.donorService.createDonor(createDonorDTO);
        return res.status(HttpStatus.OK).json({
            data: donor
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update')
    async updateDonor(@Res() res, @Param('uid') uid, @Body() createDonorDTO: CreateDonorDTO) {
        const updatedDonor = await this.donorService.updateDonor(uid, createDonorDTO);
        if ( !updatedDonor ) throw new NotFoundException('Donor Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: updatedDonor
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete')
    async deleteDonor(@Res() res, @Param('uid') uid) {
        const donor = await this.donorService.deleteDonor(uid);
        if ( !donor ) throw new NotFoundException('Donor Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }

    // Only for Admins
    // @UseGuards(JWTAuthGuard)
    // @Delete('/delete/:uid')
    // async deleteDonor(@Res() res, @Param('uid') uid) {
    //     const donor = await this.donorService.deleteDonor(uid);
    //     if ( !donor ) throw new NotFoundException('Donor Does Not Exists');
    //     return res.status(HttpStatus.NO_CONTENT).json({});
    // }


}
