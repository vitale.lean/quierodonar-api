import { Global, Module } from '@nestjs/common';
import { DonorService } from './donor.service';
import { DonorController } from './donor.controller';
import { MongooseModule } from "@nestjs/mongoose";
import { DonorSchema } from "./schema/donor.schema";
import { AppService } from 'src/app.service';

@Global()
@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Donor', schema: DonorSchema }
  ])],
  providers: [DonorService, AppService],
  controllers: [DonorController],
  exports: [DonorService]
})
export class DonorModule {}
