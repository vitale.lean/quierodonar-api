import { Injectable } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { DonorDocument } from "./schema/donor.schema";
import { CreateDonorDTO } from "./dto/donor.dto";

@Injectable()
export class DonorService {

    constructor(@InjectModel('Donor') private donorModel: Model<DonorDocument>) {}

    async getDonors(): Promise<DonorDocument[]> {
        const donors = await this.donorModel.find();
        return donors;
    }

    async getDonor(donorId: string): Promise<DonorDocument> {
        const donor = await this.donorModel.findById(donorId);
        return donor;
    }

    async getDonorByUid(uid: string): Promise<DonorDocument> {
        const donor = await this.donorModel.findOne({ 'uid': uid });
        return donor;
    }

    async getDonorByAlias(alias: string): Promise<DonorDocument> {
        const donor = await this.donorModel.findOne({ 'alias': alias });
        return donor;
    }

    async createDonor(createDonorDTO: CreateDonorDTO): Promise<DonorDocument> {
        const donor = new this.donorModel(createDonorDTO);
        await donor.save();
        return donor;
    }

    async updateDonor(donorUid: string, createDonorDTO: CreateDonorDTO): Promise<DonorDocument> {
        createDonorDTO.updated_at = new Date();
        const updatedDonor = await this.donorModel.findOneAndUpdate({ uid: donorUid }, createDonorDTO, { new: true });
        return updatedDonor;
    }

    async deleteDonor(donorUid: string): Promise<DonorDocument> {
        const donor = await this.getDonorByUid(donorUid);
        donor.deleted_at = new Date();
        donor.deleted = true;
        const softDeletedDonor = await this.donorModel.findOneAndUpdate({ uid: donorUid });
        return softDeletedDonor;
    }

}
