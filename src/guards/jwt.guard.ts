import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { AppService } from 'src/app.service';

@Injectable()
export class JWTAuthGuard implements CanActivate {
  
  constructor(private appService: AppService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    try {
      const user = await this.appService.checkJWT(request.headers.authorization);
      request.params.uid = user.uid; // Set UID
      return true;
    } catch ( e ) {
      throw new UnauthorizedException(e);
    }
  }

}
