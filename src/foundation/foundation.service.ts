import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from "mongoose";
import { InjectModel } from "@nestjs/mongoose";

import { FoundationDocument } from "./schema/foundation.schema";
import { CreateFoundationDTO } from "./dto/foundation.dto";
import { StripeService } from '../services/stripe.service';

@Injectable()
export class FoundationService {

    constructor(@InjectModel('Foundation')  private foundationModel: Model<FoundationDocument>,
                                            private stripeService: StripeService) { }

    async getFoundations(): Promise<FoundationDocument[]> {
        const foundations = await this.foundationModel.find();
        return foundations;
    }

    async getFoundation(foundationId: string, select?: string, populate?: string): Promise<FoundationDocument> {
        try {
            const foundation = await this.foundationModel.findById(foundationId).select(select).populate(populate);            
            return foundation;
        } catch ( err ) {
            throw new NotFoundException( err.response );
        }
    }

    async getFoundationByUid(foundationUid: string, select?: string): Promise<FoundationDocument> {
        try {
            const foundation = await this.foundationModel.findOne({ uid: foundationUid }).select(select);
            return foundation;
        }  catch ( err ) {
            throw new NotFoundException( err.response );
        }
    }

    async getFoundationInternal(foundationUid: string): Promise<FoundationDocument> {
        const foundation = await this.foundationModel.findOne({ uid: foundationUid }).populate('fee').select('fee name email');
        return foundation;
    }

    async getFoundationByAlias(alias: string): Promise<FoundationDocument> {
        const foundation = await this.foundationModel.findOne({ 'alias': alias });
        return foundation;
    }

    async createFoundation(createFoundationDTO: CreateFoundationDTO): Promise<FoundationDocument> {
        let foundation = new this.foundationModel(createFoundationDTO);
        await foundation.save();

        // Create Stripe Account
        const accStripe = await this.stripeService.createAccount(foundation.email);
        createFoundationDTO.stripe_id = accStripe.id;
        
        // Update Foundation Data
        foundation = await this.updateFoundation(foundation.uid, createFoundationDTO);
        return foundation;
    }

    async updateFoundation(foundationUid: string, createFoundationDTO: CreateFoundationDTO): Promise<FoundationDocument> {
        createFoundationDTO.updated_at = new Date();
        const updatedFoundation = await this.foundationModel.findOneAndUpdate({ uid: foundationUid }, createFoundationDTO, { new: true });
        return updatedFoundation;
    }

    async deleteFoundation(foundationUid: string): Promise<FoundationDocument> {
        const foundation = await this.getFoundationByUid(foundationUid);
        foundation.deleted_at = new Date();
        foundation.deleted = true;
        const softDeleteFoundation = await this.foundationModel.findOneAndUpdate({ uid: foundationUid }, foundation, { new: true });
        return softDeleteFoundation;
    }

    async createLinkStripe(uid: string): Promise<any> {
        const foundation = await this.getFoundationByUid(uid, 'stripe_id');
        const linkStripe = await this.stripeService.createLinkAccount(foundation.stripe_id);
        return linkStripe;
    }

    async getAccStripe(uid: string): Promise<any> {
        const foundation = await this.getFoundationByUid(uid, 'stripe_id');
        const accStripe = await this.stripeService.getAccount(foundation.stripe_id);
        return accStripe;
    }

}
