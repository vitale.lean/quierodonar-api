import { Global, Module } from '@nestjs/common';
import { FoundationController } from './foundation.controller';
import { FoundationService } from './foundation.service';
import { MongooseModule } from "@nestjs/mongoose";
import { FoundationSchema } from "./schema/foundation.schema";
import { AppService } from 'src/app.service';
import { StripeService } from 'src/services/stripe.service';

@Global()
@Module({
  imports: [MongooseModule.forFeature([
    { name: 'Foundation', schema: FoundationSchema },
  ])],
  controllers: [FoundationController],
  providers: [FoundationService, AppService, StripeService],
  exports: [FoundationService]
})
export class FoundationModule {}
