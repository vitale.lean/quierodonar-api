import { Controller, Get, Put, Post, Delete, Res, HttpStatus, Body, Param, NotFoundException, UseGuards } from '@nestjs/common';
import { JWTAuthGuard } from 'src/guards/jwt.guard';
import { CreateFoundationDTO } from "./dto/foundation.dto";

import { FoundationService } from "./foundation.service";

@Controller('foundation')
export class FoundationController {

    constructor(private foundationService: FoundationService) {}

    @UseGuards(JWTAuthGuard)
    @Get('/')
    async getFoundations(@Res() res) {
        const foundations = await this.foundationService.getFoundations();
        return res.status(HttpStatus.OK).json({
            data: foundations
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/id/:id')
    async getFoundation(@Res() res, @Param('id') id: string) {
        const foundation = await this.foundationService.getFoundation(id);
        if  ( !foundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: foundation
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/alias/:alias')
    async getFoundationByAlias(@Res() res, @Param('alias') alias) {
        const foundation = await this.foundationService.getFoundationByAlias(alias);
        if ( !foundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: foundation
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/uid/:uid')
    async getFoundationByUid(@Res() res, @Param('uid') uid) {
        const foundation = await this.foundationService.getFoundationByUid(uid);
        if ( !foundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: foundation
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/info')
    async getFoundationInfo(@Res() res, @Param('uid') uid) {
        const foundation = await this.foundationService.getFoundationByUid(uid);
        if ( !foundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.OK).json({
            data: foundation
        });
    }

    @UseGuards(JWTAuthGuard)
    @Post('/create')
    async createFoundation(@Res() res, @Param('uid') uid, @Body() createFoundationDTO: CreateFoundationDTO) {
        try {
            createFoundationDTO.uid = uid;
            const foundation = await this.foundationService.createFoundation(createFoundationDTO);
            return res.status(HttpStatus.OK).json({
                created: true,
                data: foundation
            });
        } catch (error) {
            throw new NotFoundException(error.message);
        }
        
    }

    @UseGuards(JWTAuthGuard)
    @Get('/linkStripe')
    async createLinkStripe(@Res() res, @Param('uid') uid) {
        const linkStripe = await this.foundationService.createLinkStripe(uid);
        return res.status(HttpStatus.OK).json({
            data: linkStripe
        });
    }

    @UseGuards(JWTAuthGuard)
    @Get('/stripe')
    async getStripeAccount(@Res() res, @Param('uid') uid) {
        const accStripe = await this.foundationService.getAccStripe(uid);
        return res.status(HttpStatus.OK).json({
            data: accStripe
        });
    }

    @UseGuards(JWTAuthGuard)
    @Put('/update')
    async updateFoundation(@Res() res, @Param('uid') uid, @Body() createFoundationDTO: CreateFoundationDTO) {
        const updatedFoundation = await this.foundationService.updateFoundation(uid, createFoundationDTO);
        if  ( !updatedFoundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.OK).json({
            updated: true,
            data: updatedFoundation
        });
    }

    @UseGuards(JWTAuthGuard)
    @Delete('/delete')
    async deleteFoundation(@Res() res, @Param('uid') uid) {
        const foundation = await this.foundationService.deleteFoundation(uid);
        if  ( !foundation ) throw new NotFoundException('Foundation Does Not Exists');
        return res.status(HttpStatus.NO_CONTENT).json({});
    }

}
