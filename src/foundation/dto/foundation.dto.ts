import { RRSS } from "../schema/rrss.schema";
import { Organization } from "../schema/organization.schema";
import { Profile } from "../schema/profile.schema";

export class CreateFoundationDTO {
    uid: string;
    readonly name: string;
    readonly country: string;
    readonly state: string;
    readonly foundation_type: number;
    readonly plan: number;
    readonly status: number;
    readonly email: string;
    readonly image: string;
    readonly alias: string;
    readonly created_at: Date;
    updated_at: Date;
    deleted_at: Date;
    readonly profile: Profile;
    readonly organization: Organization;
    readonly rrss: RRSS;
    stripe_id: string;
    readonly fee: string;
}