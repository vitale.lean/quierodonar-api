import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import * as mongoose from 'mongoose'; 

import { Organization } from "./organization.schema";
import { Profile } from "./profile.schema";
import { RRSS } from "./rrss.schema";

export type FoundationDocument = Foundation & Document;

@Schema({ versionKey: false })
export class Foundation {
    @Prop({ type: String, required: true, unique: true })
    uid: string;
    
    @Prop({ type: String, required: true })
    name: string;
    
    @Prop({ type: String, required: true })
    country: string;
    
    @Prop({ type: String, required: true })
    state: string;
    
    @Prop({ type: Number, required: true })
    foundation_type: number;
    
    @Prop({ type: Number, required: true })
    plan: number;
    
    @Prop({ type: Boolean, default: false })
    actived: boolean;
    
    @Prop({ type: Number, required: true })
    status: number;
    
    @Prop({ type: String, required: true, unique: true })
    email: string;
    
    @Prop({ type: String, required: true })
    image: string;
    
    @Prop({ type: String, required: true, unique: true })
    alias: string;
    
    @Prop({ type: Boolean, default: false, select: false })
    deleted: boolean;
    
    @Prop({ type: Date, default: Date.now, select: false })
    created_at: Date;
    
    @Prop({ type: Date, default: Date.now, select: false })
    updated_at: Date;
    
    @Prop({ type: Date, select: false })
    deleted_at: Date;
    
    @Prop({ type: Object })
    profile: Profile;
    
    @Prop({ type: Object })
    organization: Organization;
    
    @Prop({ type: Object })
    rrss: RRSS;

    @Prop({ type: String, select: false })
    stripe_id: string;

    @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Fee', default: '607e27fe71381e0f97e76469', select: false })
    fee: Types.ObjectId;
}

export const FoundationSchema = SchemaFactory.createForClass(Foundation);