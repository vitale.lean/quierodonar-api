import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProfileDocument = Profile & Document;

@Schema()
export class Profile {
    @Prop({ type: String })
    description: string;

    @Prop({ type: String })
    slogan: string;

    @Prop({ type: String })
    cover: string;

    @Prop({ type: String })
    title: string;
}

export const ProfileSchema = SchemaFactory.createForClass(Profile);