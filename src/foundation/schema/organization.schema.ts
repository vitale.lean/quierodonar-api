import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OrganizationDocument = Organization & Document;

@Schema()
export class Organization {
    @Prop({ type: String })
    name: string;

    @Prop({ type: String })
    cuit: string;

    @Prop({ type: Boolean })
    deductible: boolean;
}

export const OrganizationSchema = SchemaFactory.createForClass(Organization);