import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RRSSDocument = RRSS & Document;

@Schema()
export class RRSS {
    @Prop({ type: String })
    facebook: string;

    @Prop({ type: String })
    twitter: string;

    @Prop({ type: String })
    whatsapp: string;

    @Prop({ type: String })
    youtube: string;

    @Prop({ type: String })
    linkedin: string;

    @Prop({ type: String })
    web: string;

    @Prop({ type: String })
    phone: string;
}

export const RRSSSchema = SchemaFactory.createForClass(RRSS);