import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FoundationModule } from './foundation/foundation.module';
import { MongooseModule } from "@nestjs/mongoose";
import { DonorModule } from './donor/donor.module';
import { EventModule } from './event/event.module';
import { EventTypeModule } from './utility/event-type/event-type.module';
import { CountryModule } from './utility/country/country.module';
import { StateModule } from './utility/state/state.module';
import { StatusModule } from './utility/status/status.module';
import { FeeModule } from './utility/fee/fee.module';
import { DonateModule } from './pay/donate/donate.module';
import { OperationModule } from './pay/operation/operation.module';
import { DonorSchema } from './donor/schema/donor.schema';
import { DonorService } from './donor/donor.service';
import { SuscriptionModule } from './pay/suscription/suscription.module';

@Module({
  imports: [
              ConfigModule.forRoot({
                isGlobal: true,
              }),
              MongooseModule.forRoot(process.env.MONGOOSE, { useFindAndModify: false, useCreateIndex: true }),
              FoundationModule, 
              DonorModule, 
              EventModule, 
              EventTypeModule, 
              CountryModule, 
              StateModule, 
              StatusModule,
              FeeModule, 
              DonateModule, 
              OperationModule, SuscriptionModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
